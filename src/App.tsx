import React from 'react';
import './App.css';
import LoginForm from './features/users/LoginForm';
import TopBar from './features/header/TopBar';

const App: React.FC = () => {
  return (
    <div className="App">
        <TopBar />
        <LoginForm />
    </div>
  );
}

export default App;
